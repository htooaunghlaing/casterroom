package com.hah.android.casterroom.users

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.EditorInfo.IME_ACTION_DONE
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.hah.android.casterroom.AppDatabase
import com.hah.android.casterroom.R
import kotlinx.android.synthetic.main.activity_users.*
import kotlinx.android.synthetic.main.content_users.*
import kotlin.concurrent.thread

class UsersActivity : AppCompatActivity() {

    private lateinit var userListAdapter: UserListAdapter
    private lateinit var database: AppDatabase
    private lateinit var userDao: UserDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users)
        setSupportActionBar(toolbar)

        database = AppDatabase.getInstance(this)
        userDao = database.userDao()

        addUserButton.setOnClickListener{
            addUser()
            userNameInput.setText("")
        }

        userNameInput.setOnEditorActionListener(TextView.OnEditorActionListener{ _, actionId, _->
            if(actionId == IME_ACTION_DONE){
                addUser()
                userNameInput.setText("")
                return@OnEditorActionListener true
            }
            return@OnEditorActionListener false
        })

        userListAdapter = UserListAdapter ({
            thread { userDao.delete(it) }
        })

        userList.layoutManager = LinearLayoutManager(this)
        userList.adapter = userListAdapter

        userDao.getAll().observe(this, Observer<List<User>> {
            userListAdapter.submitList(it)
        })
    }

    private fun addUser(){
        val title = userNameInput.text.toString()

        if(title.isBlank()){
            Snackbar.make(toolbar, "User's name is required", Snackbar.LENGTH_LONG).show()
            return
        }

        val user = User(name = title)

        thread { userDao.insert(user) }
    }
}
