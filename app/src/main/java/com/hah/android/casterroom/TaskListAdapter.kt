package com.hah.android.casterroom

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_task_row.view.*

//class TaskListAdapter(private val clickListener: (Task) -> Unit) :
//    ListAdapter<Task, TaskListAdapter.ViewHolder>(DIFF_UTIL_CALLBACK) {
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        val inflater = LayoutInflater.from(parent.context)
//        return ViewHolder(inflater.inflate(R.layout.item_task_row, parent, false))
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.bind(getItem(position), clickListener)
//    }
//
//    companion object {
//
//        val DIFF_UTIL_CALLBACK = object : DiffUtil.ItemCallback<Task>() {
//
//            override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean {
//                return oldItem.id == newItem.id
//            }
//
//            override fun areContentsTheSame(oldItem: Task, newItem: Task): Boolean {
//                return oldItem == newItem
//            }
//        }
//    }
//
//    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//
//        fun bind(task: Task, clickListener: (Task) -> Unit) {
//            itemView.taskTitle.text = task.title
//            itemView.setOnClickListener { clickListener(task) }
//        }
//
//    }
//}

class TaskListAdapter(private val clickListener: (Task) -> Unit) : ListAdapter<Task, TaskListAdapter.ViewHolder>(DIFF_UTIL_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_task_row, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

//    override fun getItemCount(): Int = tasks.size

    companion object {

        val DIFF_UTIL_CALLBACK = object : DiffUtil.ItemCallback<Task>() {

            override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Task, newItem: Task): Boolean {
                return oldItem == newItem
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(task: Task, clickListener: (Task) -> Unit) {
            itemView.taskTitle.text = task.title
            itemView.setOnClickListener{ clickListener(task) }
        }

    }
}