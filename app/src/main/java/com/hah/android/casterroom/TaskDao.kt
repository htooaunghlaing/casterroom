package com.hah.android.casterroom


import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface TaskDao{

    @Insert
    fun insert(task: Task): Long

    @Insert
    fun insertAll(taskList: List<Task>): List<Long>

    @Query("SELECT * FROM Task")
    fun getAll(): LiveData<List<Task>>

    @Query("SELECT * FROM Task WHERE id = :taskId")
    fun getTask(taskId: Int): LiveData<Task>

    @Delete
    fun delete(task: Task)

    @Update
    fun update(it: Task)

}