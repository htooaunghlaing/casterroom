package com.hah.android.casterroom

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.hah.android.casterroom.users.User
import com.hah.android.casterroom.users.UserDao

@Database(version = 1, entities = [
    //list of entities
    Task::class,
    User::class
])
abstract class AppDatabase : RoomDatabase(){

    abstract fun taskDao(): TaskDao
    abstract fun userDao(): UserDao

    companion object {
        @Volatile private var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase{
            return INSTANCE ?: synchronized(this){
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }
        }

        private fun buildDatabase(context: Context) : AppDatabase{
            return Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "app-database")
                .build()
        }
    }
}