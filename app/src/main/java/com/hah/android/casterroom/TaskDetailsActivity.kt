package com.hah.android.casterroom

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.Adapter
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.annotation.AnyThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.hah.android.casterroom.users.User
import com.hah.android.casterroom.users.UserDao
import kotlinx.android.synthetic.main.content_task_details.*
import kotlinx.android.synthetic.main.item_task_row.taskTitle
import kotlin.concurrent.thread
import kotlinx.android.synthetic.main.content_task_details.taskId as taskIdInput

class TaskDetailsActivity : AppCompatActivity() {

    private lateinit var taskDao: TaskDao
    private lateinit var userDao: UserDao

    private lateinit var assigneeArrayAdapter: ArrayAdapter<UserSelectionChoice>

    private  var task: Task? = null

    private var spinnerInitialized = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_detail)
        configureAssigneeAdapter()

        taskDao = AppDatabase.getInstance(this).taskDao()
        userDao = AppDatabase.getInstance(this).userDao()

        val taskId = extractTaskId()

        taskDao.getTask(taskId).observe(this, Observer<Task> {
          if( it == null ){
              //Toast.makeText(applicationContext, R.string.could_not_load_task, Toast.LENGTH_LONG).show()
              finish()
              return@Observer
          }

            taskTitle.text = it.title
            taskIdInput.text = it.id.toString()
            assigneeUserId.text = it.userId?.toString() ?: getString(R.string.unassigned)
            taskCompletionCheckbox.isChecked = it.completed
            task = it
        })

        userDao.getAll().observe(this, Observer<List<User>> {
            it?.forEach { assigneeArrayAdapter.add(UserSelectionChoice.SelectedUser(it)) }
            assigneeArrayAdapter.add(UserSelectionChoice.Unassign)
            assignee.isEnabled = true
        })

        taskCompletionCheckbox.setOnCheckedChangeListener{ _, isChecked ->
            task?.let{
                //update the task in the DB
                it.completed = isChecked
                thread { taskDao.update(it) }
            }
        }
        
    }

    private fun configureAssigneeAdapter() {
       assigneeArrayAdapter = AssigneeAdapter(this, android.R.layout.simple_spinner_item)
        assigneeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        assigneeArrayAdapter.add(UserSelectionChoice.Instruction)
        assignee.adapter = assigneeArrayAdapter
        assignee.isEnabled = false

        assignee.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(!spinnerInitialized){
                    spinnerInitialized = true
                    return
                }

                val selectedUserChoice = assigneeArrayAdapter.getItem(position)
                when(selectedUserChoice) {
                    is UserSelectionChoice.SelectedUser -> assignUserToTask(selectedUserChoice.user)
                    UserSelectionChoice.Unassign -> unassignUserFromTask()
                }

                assignee.setSelection(0, true)
            }
        }
    }

    private fun assignUserToTask(selectedUser: User){
        task ?.let {
            it.userId = selectedUser.id
            updateTask(it)
        }
    }

    private fun unassignUserFromTask(){
        task?.let{
            it.userId = null
            updateTask(it)
        }
    }

    @AnyThread
    private fun updateTask(task: Task) =  thread{ taskDao.update(task) }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_task_details, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId){
            R.id.menu_item_delete_task -> {
                //need to implement delete here
                thread { task?.let { taskDao.delete(it) } }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun extractTaskId(): Int {
        if(!intent.hasExtra(TASK_ID)){
            throw IllegalArgumentException("$TASK_ID must be provided to start this Activity")
        }
        return intent.getIntExtra(TASK_ID, Int.MIN_VALUE)
    }

    companion object{
        private const val TASK_ID = "INSTANT_TASK_ID"

        fun launchIntent(context: Context, taskId: Int): Intent {
            val intent = Intent(context, TaskDetailsActivity::class.java)
            intent.putExtra(TASK_ID, taskId)
            return intent
        }
    }

    private sealed class UserSelectionChoice{
        object Instruction : UserSelectionChoice()
        object Unassign : UserSelectionChoice()
        data class SelectedUser(val user: User) : UserSelectionChoice()
    }

    private class AssigneeAdapter(context: Context, resource: Int) : ArrayAdapter<UserSelectionChoice>(context, resource) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            return super.getView(position, convertView, parent)
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
            return super.getDropDownView(position, convertView, parent)
        }

        private fun configureListView(position: Int, convertView: View?, parent: ViewGroup?) : View {
            val listItemView: View = convertView ?: LayoutInflater.from(context).inflate(
                android.R.layout.simple_spinner_dropdown_item,
                parent,
                false
            )

            val userSelectionChoice = getItem(position)

            val textView = listItemView.findViewById<View>(android.R.id.text1) as TextView

            textView.text = when (userSelectionChoice){
                is UserSelectionChoice.SelectedUser -> {
                    "${userSelectionChoice.user.name} (id = ${userSelectionChoice.user.id})"
                }

                UserSelectionChoice.Instruction -> context.getString(R.string.assignTaskInstruction)
                UserSelectionChoice.Unassign -> context.getString(R.string.unassign)
            }
            return listItemView
        }
    }
}
